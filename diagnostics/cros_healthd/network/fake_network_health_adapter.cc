// Copyright 2020 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "diagnostics/cros_healthd/network/fake_network_health_adapter.h"

namespace diagnostics {

FakeNetworkHealthAdapter::FakeNetworkHealthAdapter() = default;
FakeNetworkHealthAdapter::~FakeNetworkHealthAdapter() = default;

}  // namespace diagnostics
