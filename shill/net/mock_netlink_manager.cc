// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "shill/net/mock_netlink_manager.h"

namespace shill {

MockNetlinkManager::MockNetlinkManager() = default;

MockNetlinkManager::~MockNetlinkManager() = default;

}  // namespace shill
