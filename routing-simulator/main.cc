// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include <base/logging.h>

int main() {
  LOG(INFO) << "hello";
  // TODO(b/307460180): Add implementation later.
  return 0;
}
