% minijail-config-file v0

# Make sure minijail0 exits right away and won't block upstart.
i

# Create a PID namespace (process won't see any other processes).
p

# Create an IPC namespace (isolate System V IPC objects/POSIX message queues).
l

# Remount /proc read-only (prevents any messing with it).
r

# Enter new mount namespace, allows to change mounts inside jail.
ns-mount

# Creates new, empty tmp directory (technically, mounts tmpfs).
t

# Prevent that execve gains privileges, required for seccomp filters.
n

# Set the CAP_SETPCAP and CAP_SETUID capabilities to set authpolicyd-exec as
# saved UID and drops caps. This allows authpolicyd to switch to the
# authpolicyd-exec user when running Samba code or parsing data. Note that
# all caps are dropped right after startup.
c = 180

# Create a pivot_root at the target folder.
P = /mnt/empty

# Make sure mounts are remounted as secondary  mounts, so that the user's
# cryptohome can propagate into the jail. Note that
# /run/daemon-store/authpolicyd is a shared mount.
K = slave # nocheck

# Bind-mount / read-only.
bind-mount = /

# Bind-mount /dev read-only for Samba to work.
bind-mount = /dev

# Bind-mount /run read-only for Samba and D-Bus to work.
bind-mount = /run

# Bind-mount /run/authpolicyd read-write to store debug flags and auth data.
bind-mount = /run/authpolicyd,,1

# Bind-mount /run/daemon-store/authpolicyd read-write to back up auth state.
# Mount events for the user's cryptohome will propagate into our mount
# namespace. See
# https://chromium.googlesource.com/chromiumos/docs/+/HEAD/sandboxing.md#securely-mounting-cryptohome-daemon-store-folders
# for more details. In case authpolicyd starts up when the user's cryptohome
# is already mounted (e.g. after a crash), the 0x5000 option (MS_REC|MS_BIND)
# makes sure the daemon store is visible inside the namespace as well.
mount = /run/daemon-store/authpolicyd,/run/daemon-store/authpolicyd,none,0x5000

# Bind-mount /sys read-only for Samba to work.
bind-mount = /sys

# Bind-mount /var read-only for Samba to work.
bind-mount = /var

# Bind-mount /var/lib/devicesettings read-only for Samba to work.
bind-mount = /var/lib/devicesettings

# Bind-mount /var/lib/authpolicyd read-write to store daemon state.
bind-mount = /var/lib/authpolicyd,,1

# Bind-mount /var/lib/metrics,/var/lib/metrics read-write to store UMA
# metrics.
bind-mount = /var/lib/metrics,,1

# Run as authpolicyd user and group.
u = authpolicyd
g = authpolicyd

# Inherit authpolicyd's supplementary groups, in particular 'policy-readers'
# to read device policy.
G
